﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CamMove : MonoBehaviour
{
    public float speed;
    public float speed2;
    public float positionZ;
    public float positionY;
    public float positionX;
    private void Awake()
    {
        StartCoroutine(IntroGame());   
    }

    void Update()
    {
        //if(GameManager.instance.controls == true) { 
        //if (Input.GetKey(KeyCode.UpArrow))
        //{
        //    if (transform.localPosition.y < 0.4)
        //        transform.Translate(Vector3.up * speed * Time.deltaTime);
        //}
        //else if (Input.GetKey(KeyCode.LeftArrow))
        //{
        //    if (transform.localPosition.x > -0.7)
        //        transform.Translate(Vector3.left * speed2 * Time.deltaTime);
        //}
        //else if (Input.GetKey(KeyCode.DownArrow))
        //{
        //    if (transform.localPosition.y > -0.4)
        //        transform.Translate(Vector3.down * speed * Time.deltaTime);
        //}
        //else if (Input.GetKey(KeyCode.RightArrow))
        //{
        //    if (transform.localPosition.x < 0.7)
        //        transform.Translate(Vector3.right * speed2 * Time.deltaTime);
        //}

        //if (Input.GetKeyUp(KeyCode.DownArrow))
        //{
        //    transform.DOLocalMoveY(0, .5f);

        //}
        //if (Input.GetKeyUp(KeyCode.UpArrow))
        //{
        //    transform.DOLocalMoveY(0, .5f);
        //}

        //if (Input.GetKeyUp(KeyCode.RightArrow))
        //{
        //    transform.DOLocalMoveX(0, .5f);
        //}
        //if (Input.GetKeyUp(KeyCode.LeftArrow))
        //{
        //    transform.DOLocalMoveX(0, .5f);
        //}
        //}
    }
     IEnumerator IntroGame()
    {
        yield return new WaitForSeconds(2f);
        transform.DOLocalMoveZ(positionZ, 1f);
        transform.DOLocalMoveY(positionY, 1f);
        transform.DOLocalMoveX(positionX, 1f);
    }
}
