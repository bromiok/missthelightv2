﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour
{
    //public GameObject tunel;
    public GameObject player;
    //public Transform parent;
    public float time;
    public static GameManager instance;
    public bool controls;
    public Camera Hibrido;
    public Camera Vigilante;
    private bool fadeOut;
    public float slowspeed;
    public bool playerDeath;
    //Enemies
    public bool gotHit;
    //Pickman
    public bool capture;
    public bool stressed;
    public bool capture_H;

    public bool killerLight;

    int lifeCount;
    public TextMeshProUGUI lives;

    int BatteryCount;
    public TextMeshProUGUI BC;

    private void Awake()
    {
        controls = false;
        instance = this;
    }
    void Start()
    {
        //tunel.SetActive(false);
        Hibrido.enabled = true;
        Vigilante.enabled = false;
        capture = false;
    }

    void Update()
    {
        if(gotHit == true)
        {
            StartCoroutine(Recovered());
        }
        if (Input.GetMouseButtonDown(1))
        {
            Hibrido.enabled = !Hibrido.enabled;
            Vigilante.enabled = !Vigilante.enabled;
        }
        if(Hibrido.enabled)
        {
            controls = true;
        }
        else
        {
            controls = false;
        }
        if (capture == true)
        {
            capture = false;
            EnemyController.instance.GotAway();
        }
        if(playerDeath == true)
        {
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }

        if(killerLight == true)
        {
            StartCoroutine(Recovered());
        }

        lives.text = lifeCount.ToString();
        lifeCount = Stressness.instance.s_level;

        BC.text = BatteryCount.ToString();
        BatteryCount = Battery.instance.Cargas;

    }
    //Tiempo de escape
    public void VictoryScene()
    {
        SceneManager.LoadScene("Victory");
    }
    public void ReloadScene()
    { 
        SceneManager.LoadScene("Nivel_01HybridV4");
    }
    public IEnumerator Recovered()
    {
        yield return new WaitForSeconds(1);
        gotHit = false;
    }
}
