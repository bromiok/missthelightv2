﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MataEnemigos : MonoBehaviour
{
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "pickman")
        {
            Destroy(gameObject);
        }
        if (other.gameObject.tag == "player")
        {
            Destroy(other.gameObject);
        }
    }
}
