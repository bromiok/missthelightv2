﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeaconController : MonoBehaviour
{
    public GameObject[] call;
    public float screamradious;
    public bool screaming;
    Transform player_a;

    public void Start()
    {
        player_a = PlayerManager.instance.playerH.transform;
        screaming = false;
    }
    public void Update()
    {
        float distance = Vector3.Distance(player_a.position, transform.position);
        if (distance <= screamradious)
        {
            StopCoroutine("KeepScream");
            screaming = true;
        }
        if (distance >= screamradious)
        {
            StartCoroutine("KeepScream");
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, screamradious);
    }
    IEnumerator KeepScream()
    {
        yield return new WaitForSeconds(5);
        screaming = false;
    }

}
