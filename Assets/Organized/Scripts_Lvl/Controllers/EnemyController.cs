﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    public static EnemyController instance;
    [Header("AI")]
    Transform player_a;
   
    public bool follow1;
    public bool follow2;
    public bool im1, im2;
    NavMeshAgent agent;

    [Header("Beacon")]
    public GameObject myBeacon;
    public Vector3 callingB;
    public bool beacon;
    public bool haveBeacon;

    [Header("Action")]
    public Vector3 GoThere;
    public float lookradious;
    public float actionradious;
    public float speed;
    public float slowspeed;
    public bool someone;
    public bool location;
  
    [Header("Patrol")]
    public bool middleTarget;
    public bool threeDestinations;
    public bool twoDestinations;
    public bool noPatrol;
    private Vector3 originalpos;
    public bool patrolTime;
    public GameObject target;
    private Vector3 targetPos;
    public GameObject target2;
    private Vector3 target2Pos;
    public GameObject target3;
    private Vector3 target3Pos;
    public int destiny;


    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        location = false;
        patrolTime = true;
        destiny = 1;
        someone = false;
        slowspeed = 0.2f;
        player_a = PlayerManager.instance.playerH.transform;
        agent = GetComponent<NavMeshAgent>();
        originalpos = transform.position;
        
    }
    private void Update()
    {
        if (haveBeacon)
        {
            callingB = myBeacon.transform.position;
        }
        if (haveBeacon == false)
        {
            myBeacon = null;
        }
        Find();
        if (GameManager.instance.gotHit == true)
        {
            Escaped();
        }
        if (noPatrol == true)
        {
            target = null;
            target2 = null;
            target3 = null;
        }
        if (twoDestinations == true)
        {
            targetPos = target.transform.position;
            target2Pos = target2.transform.position;
            target3 = null;
        }
        if (threeDestinations == true)
        {
            targetPos = target.transform.position;
            target2Pos = target2.transform.position;
            target3Pos = target3.transform.position;
        }
        if (someone == false)
        {
            //Patrol
            if (destiny == 1)
            {
                agent.speed = speed;
                agent.SetDestination(targetPos);
            }
            else if (destiny == 2)
            {
                agent.speed = speed;
                agent.SetDestination(target2Pos);
            }
            else if (destiny == 3)
            {
                agent.speed = speed;
                agent.SetDestination(target3Pos);
            }
        }
        
        GotAway();
       
        if (GameManager.instance.Vigilante.enabled)
        {
            agent.speed = slowspeed;
        }

        if(GameManager.instance.Hibrido.enabled)
        {
            float distanceAttack = Vector3.Distance(player_a.position, transform.position);
            if(distanceAttack <= actionradious && Hibrido.instance.safe == false)
            {
                agent.speed = 5;
            }
            else
            {
                agent.speed = speed;
            }
        }
        if (haveBeacon) { 
            if (myBeacon.GetComponent<BeaconController>().screaming && someone == false)
            {            
                Calling();
            }
            if (myBeacon.GetComponent<BeaconController>().screaming == false && someone == false)
            {
                GotAway();
            }
        }

    }
    private void FixedUpdate()
    {
        CheckSpace();
        Go();
    }
    public void Calling()
    {
        agent.SetDestination(callingB = new Vector3 (callingB.x - 2, callingB.y, callingB.z -2));
    }
    //Ve por el
    public void Find()
    {
        if (GameManager.instance.capture == false)
        {
            float distance = Vector3.Distance(player_a.position, transform.position);
            if (distance <= lookradious && Hibrido.instance.safe == false)
            {
               
                someone = true;
                patrolTime = false;
                if(EnemyManager.instance.onetaken == false && EnemyManager.instance.bothtaken == false) {
                    im1 = true;
                    EnemyManager.instance.enemiesAttacking = 1;
                    follow1 = true;
                }
                if (EnemyManager.instance.onetaken == true && im1 == false && EnemyManager.instance.bothtaken == false)
                { 
                    im2 = true;
                    follow2 = true;
                    EnemyManager.instance.enemiesAttacking = 2;
                }
            }
        }
    }
    public void Go()
    {
        if (follow1)
        {
            agent.SetDestination(GoThere = new Vector3 (player_a.position.x - .5f, player_a.position.y, player_a.position.z));
        }
        if (follow2)
        {
            agent.SetDestination((GoThere = new Vector3(player_a.position.x + .5f, player_a.position.y, player_a.position.z)));
        }
    }
    public void CheckSpace()
    {
        if (EnemyManager.instance.enemiesAttacking == 1)
        {
            if (im1)
            {
                EnemyManager.instance.enemies1 = gameObject;
                im2 = false;
                follow2 = false;
            }
        }
        if (EnemyManager.instance.enemiesAttacking == 2)
        {
            if (im2)
            {
                EnemyManager.instance.enemies2 = gameObject;
            }
        }
    }
    //regresa primera posicion
    public void GotAway()
    {
        float distance = Vector3.Distance(player_a.position, transform.position);
        if (noPatrol)
        {
            if (distance >= lookradious || Hibrido.instance.safe == true)
            {
                agent.speed = speed;
                agent.SetDestination(originalpos);
                someone = false;
                if (im1)
                {
                    im1 = false;
                    EnemyManager.instance.onetaken = false;
                    EnemyManager.instance.enemiesAttacking = 0;
                    follow1 = false;
                }
                if (im2)
                {
                    follow2 = false;
                    EnemyManager.instance.enemiesAttacking = 1;
                    im2 = false;
                }
            }
        }
        if (threeDestinations == true && patrolTime == false || twoDestinations == true && patrolTime == false )
        {
            if (distance >= lookradious  || Hibrido.instance.safe == true)
            {
                agent.speed = speed;
                agent.SetDestination(target.transform.position);
                someone = false;
                patrolTime = true;
            }
        }
    }
    public void Escaped()
    {
        StartCoroutine(NoSpeed());
        agent.speed = speed;
        someone = false;
        patrolTime = true;

        if (noPatrol)
        {
            agent.SetDestination(originalpos);
        }
        if (threeDestinations == true || twoDestinations == true )
        {
            agent.SetDestination(target.transform.position);
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookradious);
        Gizmos.DrawWireSphere(transform.position, actionradious);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "target")
        {

            destiny = 2;
            middleTarget = true;
        }
        if (other.tag == "target2" && middleTarget == true)
        {
            destiny = 3;
        }
        if (other.tag == "target3")
        {
            destiny = 1;
            middleTarget = false;
        }
        if (other.tag == "2target")
        {
            destiny = 2;
        }
        if (other.tag == "2target2")
        {
            destiny = 1;
        }
        if (other.gameObject.tag == "light")
        {
            agent.speed = 0;
        }
        if (other.gameObject.tag == "deathZone")
        {

            if (im1)
            {
                im1 = false;
                follow1 = false;
                EnemyManager.instance.enemiesAttacking = 0;
            }
            if (im2)
            {
                im2 = false;
                follow2 = false;
                EnemyManager.instance.enemiesAttacking = 1;
            }
            Destroy(gameObject);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "light")
        {
            agent.speed = 0;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "light")
        {
            agent.speed = speed;
        }
    }
     public IEnumerator NoSpeed()
    {
        agent.isStopped = true;
        yield return new WaitForSeconds(1);
        agent.isStopped = false;
    }
}
