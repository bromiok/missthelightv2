﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class StalkerController : MonoBehaviour
{
    public float SusanaDistancia;

    public static StalkerController instance;
    public float lookradious;
    Transform player_a;
    NavMeshAgent agent;
    public float speed;
    public float slowspeed;
    public bool someone;

    [Header("Posicion")]
    private Vector3 GoThere;
    private Vector3 originalpos;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        someone = false;
        slowspeed = 0.2f;
        player_a = PlayerManager.instance.playerH.transform;
        agent = GetComponent<NavMeshAgent>();
        originalpos = transform.position;
    }
    void Update()
    {
        Find();
        GotAway();
        if(someone == true)
        {
            StartCoroutine("Muerte");
        }
        if (someone == false)
        {
            StopCoroutine("Muerte");
        }
    }

    //Ve por el
    void Find()
    {

        float distance = Vector3.Distance(player_a.position, transform.position);
        if (distance <= lookradious && Hibrido.instance.safe == false)
        {
            if (Vector3.Distance(player_a.position, transform.position) > SusanaDistancia)
            {
                agent.SetDestination(GoThere = new Vector3(player_a.position.x - SusanaDistancia, player_a.position.y, player_a.position.z - SusanaDistancia));
                agent.speed = speed;
                someone = true;
            }
        }
    }

    //regresa primera posicion
    public void GotAway()
    {
        float distance = Vector3.Distance(player_a.position, transform.position);
        if (distance >= lookradious)
        {
            agent.speed = speed;
            agent.SetDestination(originalpos);
            someone = false;
        }
    }
    //detecta player
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookradious);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "light")
        {
            agent.speed = 0;
        }
        if (other.gameObject.tag == "deathZone")
        {
            Destroy(gameObject);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "light")
        {
            agent.speed = speed;
        }
    }
    IEnumerator Muerte()
    {
        yield return new WaitForSeconds(10);
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }
}
