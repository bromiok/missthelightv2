﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hibrido : MonoBehaviour
{
    Quaternion rotation;
    public static Hibrido instance;
    public bool safe;

    [Header("Checkpoints")]
    public bool initialCheck;
    public bool breakerCheck1;
    public bool breakerCheck2;
    public bool breakerCheck3;
    public bool breakerCheck4;
    public bool breakerCheck5;
    public bool breakerCheck6;
    public bool breakerCheck7;
    public bool breakerCheck8;

    [Header("Movement")]
    public float speed = 0;
    float maxSpeed = 7;
    float rest = 0;
    float acceleration = 3f;
    float deceleration = 3f;
    public bool left, right, up, down;


    public AudioSource Punch;

    private void Awake()
    {
        instance = this;
        rotation = transform.rotation;
    }

    void Start()
	{
        initialCheck = true;
        CheckPoint();
	}

    void LateUpdate()
    {
        transform.rotation = rotation;
    }

    void Update()
    {

        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
        {
            if (speed < maxSpeed)
            {
                speed += 1;
            }
        }
        else
        {
            if (speed > 0)
            {
                speed -= 1;
            }
            if (speed < 0)
            {
                speed = 0;
            }
        }
        if (GameManager.instance.controls == true)
        {
            if (Input.GetKey(KeyCode.W))
            {
                transform.Translate(Vector3.up * speed * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.A))
            {
                transform.Translate(Vector3.left * speed * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.S))
            {
                transform.Translate(Vector3.down * speed * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.D))
            {
                transform.Translate(Vector3.right * speed * Time.deltaTime);
            }
        }
        //if (Input.GetKeyDown(KeyCode.W))
        //{
        //    right = false;
        //    left = false;
        //    up = true;
        //    down = false;
        //}
        //if (Input.GetKeyDown(KeyCode.A))
        //{
        //    right = false;
        //    left = true;
        //    up = false;
        //    down = false;
        //}
        //if (Input.GetKeyDown(KeyCode.S))
        //{
        //    right = false;
        //    left = false;
        //    up = false;
        //    down = true;
        //}
        //if (Input.GetKeyDown(KeyCode.D))
        //{
        //    right = true;
        //    left = false;
        //    up = false;
        //    down = false;
        //}
        //if (Input.GetKeyUp(KeyCode.W))
        //{
        //    up = false;         
        //}
        //if (Input.GetKeyUp(KeyCode.A))
        //{
        //    left = false;
        //}
        //if (Input.GetKeyUp(KeyCode.S))
        //{
        //    down = false;
        //}
        //if (Input.GetKeyUp(KeyCode.D))
        //{
        //    right = false;
        //}

        if (Stressness.instance.s_level < 0)
        {
            GameManager.instance.ReloadScene();
        }
    }

    void CheckPoint()
    {
        if (initialCheck == true && GameManager.instance.killerLight)
        {
            if (Stressness.instance.s_level > 0)
            {
                this.gameObject.transform.position = new Vector3(-44.5f, 0f, -1.5f);
            }
        }

        if (breakerCheck1 == true && GameManager.instance.killerLight)
        {
            if (Stressness.instance.s_level > 0)
            {
                this.gameObject.transform.position = new Vector3(-3.5f, 0f, -18.5f);
            }

        }

        if (breakerCheck2 == true && GameManager.instance.killerLight)
        {
            if (Stressness.instance.s_level > 0)
            {
                this.gameObject.transform.position = new Vector3(-36.5f, 0f, 24.5f);
            }
        }

        if (breakerCheck3 == true && GameManager.instance.killerLight)
        {
            if (Stressness.instance.s_level > 0)
            {
                this.gameObject.transform.position = new Vector3(-40f, 0f, -45f);
            }
        }

        if (breakerCheck4 == true && GameManager.instance.killerLight)
        {
            if (Stressness.instance.s_level > 0)
            {
                this.gameObject.transform.position = new Vector3(24f, 0f, -35f);
            }
        }

        if (breakerCheck5 == true && GameManager.instance.killerLight)
        {
            if (Stressness.instance.s_level > 0)
            {
                this.gameObject.transform.position = new Vector3(65f, 0f, -15f);
            }
        }

        if (breakerCheck6 == true && GameManager.instance.killerLight)
        {
            if (Stressness.instance.s_level > 0)
            {
                this.gameObject.transform.position = new Vector3(-17f, 0f, -55f);
            }
        }

        if (breakerCheck7 == true && GameManager.instance.killerLight)
        {
            if (Stressness.instance.s_level > 0)
            {
                this.gameObject.transform.position = new Vector3(107f, 0f, -6f);
            }
        }

        if (breakerCheck8 == true && GameManager.instance.killerLight)
        {
            if (Stressness.instance.s_level > 0)
            {
                this.gameObject.transform.position = new Vector3(115, 0f, 19f);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "pickman" && GameManager.instance.gotHit == false)
        {
            GameManager.instance.capture = true;
            GameManager.instance.gotHit = true;
            Stressness.instance.s_level = (Stressness.instance.s_level - 1);
            Punch.Play();
            StartCoroutine(Safe());
        }

        if (other.tag == "hunter")
        {
            GameManager.instance.playerDeath = true;
        }

        if (other.gameObject.tag == "deathZone")
        {
            GameManager.instance.killerLight = true;
            Stressness.instance.s_level = (Stressness.instance.s_level - 1);
            CheckPoint();
            Punch.Play();
            StartCoroutine(Safe());
        }

        if (other.gameObject.tag == "light")
        {
            GameManager.instance.killerLight = true;
            Stressness.instance.s_level = (Stressness.instance.s_level - 1);
            CheckPoint();
            Punch.Play();
            StartCoroutine(Safe());
        }

        if (other.tag == "Victory")
        {
            GameManager.instance.VictoryScene();
        }

        if (other.tag == "Check1")
        {
            initialCheck = false;
            breakerCheck1 = true;
            breakerCheck2 = false;
            breakerCheck3 = false;
            breakerCheck4 = false;
            breakerCheck5 = false;
            breakerCheck6 = false;
            breakerCheck7 = false;
            breakerCheck8 = false;
        }

        if (other.tag == "Check2")
        {
            initialCheck = false;
            breakerCheck1 = false;
            breakerCheck2 = true;
            breakerCheck3 = false;
            breakerCheck4 = false;
            breakerCheck5 = false;
            breakerCheck6 = false;
            breakerCheck7 = false;
            breakerCheck8 = false;

        }

        if (other.tag == "Check3")
        {
            initialCheck = false;
            breakerCheck1 = false;
            breakerCheck2 = false;
            breakerCheck3 = true;
            breakerCheck4 = false;
            breakerCheck5 = false;
            breakerCheck6 = false;
            breakerCheck7 = false;
            breakerCheck8 = false;
        }

        if (other.tag == "Check4")
        {
            initialCheck = false;
            breakerCheck1 = false;
            breakerCheck2 = false;
            breakerCheck3 = false;
            breakerCheck4 = true;
            breakerCheck5 = false;
            breakerCheck6 = false;
            breakerCheck7 = false;
            breakerCheck8 = false;
        }

        if (other.tag == "Check5")
        {
            initialCheck = false;
            breakerCheck1 = false;
            breakerCheck2 = false;
            breakerCheck3 = false;
            breakerCheck4 = false;
            breakerCheck5 = true;
            breakerCheck6 = false;
            breakerCheck7 = false;
            breakerCheck8 = false;

        }

        if (other.tag == "Check6")
        {
            initialCheck = false;
            breakerCheck1 = false;
            breakerCheck2 = false;
            breakerCheck3 = false;
            breakerCheck4 = false;
            breakerCheck5 = false;
            breakerCheck6 = true;
            breakerCheck7 = false;
            breakerCheck8 = false;
        }

        if (other.tag == "Check7")
        {
            initialCheck = false;
            breakerCheck1 = false;
            breakerCheck2 = false;
            breakerCheck3 = false;
            breakerCheck4 = false;
            breakerCheck5 = false;
            breakerCheck6 = false;
            breakerCheck7 = true;
            breakerCheck8 = false;
        }

        if (other.tag == "Check8")
        {
            initialCheck = false;
            breakerCheck1 = false;
            breakerCheck2 = false;
            breakerCheck3 = false;
            breakerCheck4 = false;
            breakerCheck5 = false;
            breakerCheck6 = false;
            breakerCheck7 = false;
            breakerCheck8 = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        //if (other.gameObject.tag == "light")
        //{
        //    maxSpeed = 100f;
        //    moveSpeed = 100;
        //}
    }

    IEnumerator Safe()
    {
        safe = true;
        yield return new WaitForSeconds(2);
        safe = false;
    }
}
