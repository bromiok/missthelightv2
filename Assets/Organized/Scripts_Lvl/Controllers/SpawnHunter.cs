﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnHunter : MonoBehaviour
{

    public Vector3 spw;
    public GameObject Hunter;
    public int spwChose;
    Transform player_a;
    public GameObject spwn1;
    public GameObject spwn2;
    public GameObject spwn3;
    public float time;

    private void Awake()
    {
        StartCoroutine(Appear());
    }
    private void Start()
    {
        spwChose = Random.Range(0, 2);
        player_a = PlayerManager.instance.playerH.transform;
    }
    private void Update()
    {
        if(spwChose == 0)
        {
            spw.x = spwn1.transform.position.x;
            spw.y = 0;
            spw.z = spwn1.transform.position.z;
        }
        if (spwChose == 1)
        {
            spw.x = spwn2.transform.position.x;
            spw.y = 0;
            spw.z = spwn2.transform.position.z;
        }
        if (spwChose == 2)
        {
            spw.x = spwn3.transform.position.x;

            spw.z = spwn3.transform.position.z;
        }
    }
    IEnumerator Appear()
    {
        yield return new WaitForSeconds(time);
        Instantiate(Hunter, new Vector3(spw.x, spw.y, spw.z), Quaternion.identity);
    }
}
