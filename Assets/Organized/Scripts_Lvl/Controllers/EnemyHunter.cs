﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyHunter : MonoBehaviour
{
    public Transform player;
    public float moveSpeed;

    // Start is called before the first frame update
    void Start()
    {
        moveSpeed = 1f;
        player = PlayerManager.instance.playerH.transform;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, player.position, moveSpeed * Time.deltaTime);

    }
}
