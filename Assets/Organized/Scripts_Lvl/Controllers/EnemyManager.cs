﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public static EnemyManager instance; 
    public int enemiesAttacking = 0;
    public GameObject enemies1;
    public GameObject enemies2;
    public bool onetaken, bothtaken;
    public bool BScreams;

    private void Awake()
    {
        instance = this;
    }
    public void Update()
    {
        if(enemiesAttacking == 0)
        {
            enemies1 = null;
            enemies2 = null;
            onetaken = false;
            bothtaken = false;
        }
        if (enemiesAttacking == 1)
        {
            enemies2 = null;
            onetaken = true;
            bothtaken = false;
        }
        if (enemiesAttacking == 2)
        {
            bothtaken = true;
            onetaken = true;
        }
    }
}
