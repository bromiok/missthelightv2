﻿using UnityEngine;

public class CamFollow : MonoBehaviour
{
    public Transform target;

    private float smoothSpeed = 0.25f;
    private Vector3 velocity;
    private Vector3 pos;
    public float yPos;

    private void LateUpdate()
    {
        transform.position = Vector3.SmoothDamp(transform.position, target.position, ref velocity, smoothSpeed);
        pos = transform.position;
        pos.y = yPos;
        transform.position = pos;
    }

    //void FixedUpdate()
    //{
    //    Vector3 desiredPosition = target.position + offset;
    //    Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
    //    transform.position = smoothedPosition;

    //    transform.LookAt(target);
    //}
}
