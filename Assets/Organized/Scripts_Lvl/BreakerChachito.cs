﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakerChachito : MonoBehaviour
{
    public static BreakerChachito instance;
    public bool CanActivate;
    public void Awake()
    {
        instance = this;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "player")
        {
            CanActivate = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "player")
        {
            CanActivate = false;
        }
    }
}
