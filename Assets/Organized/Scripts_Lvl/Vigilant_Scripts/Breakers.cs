﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Breakers : MonoBehaviour
{
    public static Breakers instance;

    public GameObject cable;
    public GameObject[] luces;
    public GameObject[] puertas;
    public AudioSource onSound;
    public AudioSource onLine;
    public Color newColor;
    public Color oldColor;

    public bool onBreaker;
    public bool turnOn;

    void Awake()
    {
        instance = this;
    }

    void BreakerActivity()
    {
        if (onBreaker == true && turnOn == false)
        {
            onLine.Play();
            onSound.Play();
          
            CambioColor(newColor, cable);
            foreach (GameObject lights in luces)
            {
                lights.SetActive(false);
            }
            foreach (GameObject puerta in puertas)
            {
                puerta.SetActive(false);
            }
        } else if (onBreaker == true && turnOn == true)
        {
            onLine.Play();
            onSound.Play();
           
            CambioColor(newColor, cable);
            foreach (GameObject lights in luces)
            {
                lights.SetActive(true);
            }
            foreach (GameObject puerta in puertas)
            {
                puerta.SetActive(false);
            }
        }
    }

    public void CambioColor(Color color, GameObject cable)
    {
        Renderer[] renders = cable.GetComponentsInChildren<Renderer>();
        foreach (Renderer r in renders)
        {
            r.material.color = color;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.tag == "player")
        {
            if (Input.GetKey(KeyCode.E))
            {
                onLine.Play();
                onBreaker = true;
                onSound.Play();
                BreakerActivity();
                
            }
        }
    }
}
