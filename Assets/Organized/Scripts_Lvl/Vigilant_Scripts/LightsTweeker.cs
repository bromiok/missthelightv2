﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightsTweeker : MonoBehaviour
{
    //public bool NeedsDir;
    //public bool B_NeedsDir;
    //public bool B_FreeOfDir;

    //public bool paralelIsOn;
    //public bool paralelBreaker;


    //public GameObject lightRight;
    //public GameObject lightLeft;

    //public GameObject lightBreaker01;
    //public GameObject lightBreaker02;

    //public GameObject freeLightBreaker;

    //public GameObject paralelLightIsOn1;
    //public GameObject paralelLightIsOn2;
    //public GameObject paralelLightIsOn3;
    //public GameObject paralelLightIsOn4;

    //public GameObject paralelLightBreaker1;
    //public GameObject paralelLightBreaker2;
    //public GameObject paralelLightBreaker3;
    //public GameObject paralelLightBreaker4;

    public bool FreeOfDir;

    public Sprite OffButton;
    public Sprite OnButton;

    //public Hola cambioCorriente;

    public GameObject simpleLight;

    public GameObject[] brothers;

    public bool toggleBool;
    public bool startsOn;
    public bool imBro;

    public AudioSource click;

    private void Start()
    {
        toggleBool = false;

        if (startsOn && FreeOfDir == false)
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = OnButton;
            simpleLight.SetActive(true);
        }
    }

    void Update()
    {
        if (Battery.instance.Cargas < 1)
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = OffButton;
            simpleLight.SetActive(false);
            FreeOfDir = true;
            toggleBool = false;
            startsOn = false;
        }
    }

    private void OnMouseDown()
    {
        click.Play();
        toggleBool = !toggleBool;

        if (FreeOfDir && Onlight.instance.isOn == true)
        {
            if(Battery.instance.Cargas >= 1)
            {
                ChangeSprite();
                simpleLight.SetActive(toggleBool);
            }

            if (toggleBool == true)
            {
                Battery.instance.RestarCargas();
            }
        }

        if (startsOn && FreeOfDir == false && Onlight.instance.isOn == true)
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = OffButton;
            simpleLight.SetActive(false);
            FreeOfDir = true;
            toggleBool = false;
            startsOn = false;
        }

        if (imBro && Onlight.instance.isOn == true)
        {
            if (imBro == true && startsOn == true)
            {
                this.gameObject.GetComponent<SpriteRenderer>().sprite = OffButton;
                simpleLight.SetActive(false);
                toggleBool = false;
                startsOn = false;
            } else if (imBro == true && startsOn == false)
            {
                ChangeSprite();
                simpleLight.SetActive(toggleBool);
            }
            
        }

        //if (FreeOfDir && startsOn && Onlight.instance.isOn == true)
        //{
        //    simpleLight.SetActive(false);
        //}


        //if (NeedsDir && Onlight.instance.isOn == true && cambioCorriente.goRight == true)
        //{
        //    lightRight.SetActive(toggleBool);
        //    lightLeft.SetActive(false);
        //}
        //else if (NeedsDir && Onlight.instance.isOn == true && cambioCorriente.goRight == false)
        //{
        //    lightLeft.SetActive(toggleBool);
        //    lightRight.SetActive(false);
        //}

    }

    private void ChangeSprite()
    {
        if (toggleBool == false)
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = OffButton;
        }
        else
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = OnButton;
        }


        foreach (GameObject bro in brothers)
        {
            if (toggleBool == true && imBro == true)
            {
                bro.gameObject.GetComponent<SpriteRenderer>().sprite = OnButton;
            }
            else if (toggleBool == false && imBro == true)
            {
                bro.gameObject.GetComponent<SpriteRenderer>().sprite = OffButton;
            }
        }
    }

    //    // Para las luces que requieren dirección en el breaker
    //    if (chainedToBreaker && miBreaker.breakerIsOn == true && miBreaker.goRight == true)
    //    {
    //        EventoBreakerCambio();
    //    }
    //    else if (chainedToBreaker && miBreaker.breakerIsOn == true && miBreaker.goRight == false)
    //    {
    //        EventoBreakerCambio();
    //    }


    //    // Para las luces que no requieran dirección en el breaker
    //    if (freeOfChainsBreaker && miBreaker.breakerIsOn == true)
    //    {
    //        freeLightBreaker.SetActive(toggleBool);
    //    }
    //    else if (freeOfChainsBreaker && miBreaker.breakerIsOn == false)
    //    {
    //        freeLightBreaker.SetActive(toggleBool);
    //    }


    //    // Para las luces que actuan de forma paralela
    //    if (paralelIsOn && Onlight.instance.isOn == true && cambioCorriente.goRight == false)
    //    {
    //        paralelLightIsOn1.SetActive(toggleBool);
    //        if (paralelLightIsOn1.activeSelf == true)
    //        {
    //            paralelLightIsOn2.SetActive(true);
    //        }
    //        else if (paralelLightIsOn1.activeSelf == false)
    //        {
    //            paralelLightIsOn2.SetActive(false);
    //        }
    //    }
    //    else if (paralelIsOn && Onlight.instance.isOn == true && cambioCorriente.goRight == true)
    //    {
    //        paralelLightIsOn3.SetActive(toggleBool);
    //        if (paralelLightIsOn3.activeSelf == true)
    //        {
    //            paralelLightIsOn4.SetActive(true);
    //        }
    //        else if (paralelLightIsOn3.activeSelf == false)
    //        {
    //            paralelLightIsOn4.SetActive(false);
    //        }
    //    }


    //    // Para las luces que actuen de forma paralela en el breaker
    //    if (paralelBreaker && miBreaker.breakerIsOn == true && miBreaker.goRight == true)
    //    {
    //        paralelLightBreaker1.SetActive(toggleBool);
    //        if (paralelLightBreaker1.activeSelf == true)
    //        {
    //            paralelLightBreaker2.SetActive(true);
    //        }

    //    }
    //    else if (paralelBreaker && miBreaker.breakerIsOn == true && miBreaker.goRight == false)
    //    {
    //        paralelLightBreaker3.SetActive(toggleBool);
    //        if (paralelLightBreaker3.activeSelf == true)
    //        {
    //            paralelLightBreaker4.SetActive(true);
    //        }
    //    }
    //}


    //public void EventoCambioDir()
    //{
    //    if (paralelIsOn && Onlight.instance.isOn == true && cambioCorriente.goRight == false)
    //    {
    //        paralelLightIsOn1.SetActive(true);
    //        paralelLightIsOn2.SetActive(true);
    //        paralelLightIsOn3.SetActive(false);
    //        paralelLightIsOn4.SetActive(false);
    //    }
    //    else if (paralelIsOn && Onlight.instance.isOn == true && cambioCorriente.goRight == true)
    //    {
    //        paralelLightIsOn1.SetActive(false);
    //        paralelLightIsOn2.SetActive(false);
    //        paralelLightIsOn3.SetActive(toggleBool);
    //        paralelLightIsOn4.SetActive(toggleBool);
    //    }
    //}

    //public void EventoBreakerCambio()
    //{
    //    if (chainedToBreaker && miBreaker.breakerIsOn == true && miBreaker.goRight == true)
    //    {
    //        lightBreaker01.SetActive(true);
    //        lightBreaker02.SetActive(false);
    //    }
    //    else if (chainedToBreaker && miBreaker.breakerIsOn == true && miBreaker.goRight == false)
    //    {
    //        lightBreaker01.SetActive(false);
    //        lightBreaker02.SetActive(true);
    //    }

    //void deactivateIsOnAssets()
    //{
    //    if (light01 == null || light02 == null || lightBreaker01 == null ||
    //       lightBreaker02 == null || freeLightIsOn == null || freeLightBreaker == null ||
    //       paralelLightIsOn1 == null || paralelLightIsOn2 == null || paralelLightIsOn3 == null ||
    //       paralelLightIsOn4 == null || paralelLightBreaker1 == null || paralelLightBreaker2 == null ||
    //       paralelLightBreaker3 == null || paralelLightBreaker4 == null || miBreaker == null)

    //    {
    //        return;
    //    }

    //    toggleBool = false;
    //    freeLightIsOn.SetActive(false);
    //    paralelLightIsOn1.SetActive(false);
    //    paralelLightIsOn2.SetActive(false);
    //    paralelLightIsOn3.SetActive(false);
    //    paralelLightIsOn4.SetActive(false);

    //}
}