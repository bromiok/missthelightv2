﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Onlight : MonoBehaviour
{
    public static Onlight instance;

    public bool isOn;

    public GameObject[] Blinkers;
    public GameObject[] lightsR1;
    public GameObject[] lightsR2;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        StartCoroutine(offBlink());
        StartCoroutine(ritmoOff());
    }

    //private void Update()
    //{
    //    foreach(GameObject lightsR in lightsR1)
    //    {
    //        if (lightsR.activeSelf == false)
    //        {
    //            StartCoroutine(ritmo2On());
    //        } else
    //        {
    //            StartCoroutine(ritmo2Off());
    //        }
    //    }

    //    foreach (GameObject lightsR in lightsR2)
    //    {
    //        if (lightsR.activeSelf == false)
    //        {
    //            StartCoroutine(ritmo1On());
    //        }
    //        else
    //        {
    //            StartCoroutine(ritmo1Off());
    //        }
    //    }
    //}

    IEnumerator offBlink()
    {
        int blinking = Random.Range(0, 2);
        yield return new WaitForSeconds(blinking);
        foreach (GameObject light in Blinkers)
        {
            light.SetActive(false);
        }
        StartCoroutine(onBlink());
    }

    IEnumerator onBlink()
    {
        int blinking = Random.Range(0, 3);
        yield return new WaitForSeconds(blinking);
        foreach (GameObject light in Blinkers)
        {
            light.SetActive(true);
        }
        StartCoroutine(offBlink());
    }

    IEnumerator ritmoOn()
    {
        yield return new WaitForSeconds(1);
        foreach (GameObject light1 in lightsR1)
        {
            light1.SetActive(true);
        }

        foreach (GameObject light2 in lightsR2)
        {
            light2.SetActive(false);
        }

        StartCoroutine(ritmoOff());
    }

    IEnumerator ritmoOff()
    {
        yield return new WaitForSeconds(1);
        foreach (GameObject light1 in lightsR1)
        {
            light1.SetActive(false);
        }

        foreach(GameObject light2 in lightsR2)
        {
            light2.SetActive(true);
        }

        StartCoroutine(ritmoOn());
    }

}
