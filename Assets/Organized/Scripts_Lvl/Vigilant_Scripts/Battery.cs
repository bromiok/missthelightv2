﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battery : MonoBehaviour
{

    public static Battery instance;

    public int Cargas;
    public bool Recargando;

    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
    }

    void Start()
    {
        Cargas = 5;
        Recargando = false;
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Cargas == 0 && Recargando == false)
        {
            Recargando = true;
            StartCoroutine(CargandoCargas());
            
        }
        
    }

    public void RestarCargas()
    {
        Cargas = Cargas - 1;
    }

    public IEnumerator CargandoCargas()
    {
        yield return new WaitForSeconds(3.0f);
        Cargas = Cargas + 1;
        
        if(Cargas != 5)
        {
            StartCoroutine(CargandoCargas());

        }
        else
        {
            Recargando = false;
        }

    }

}
