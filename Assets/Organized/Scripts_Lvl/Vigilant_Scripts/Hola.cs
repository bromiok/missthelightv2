﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Hola : MonoBehaviour
{
    //Variables publicas para determinar cuál cable es el que cambia de color
    public GameObject cable_1;
    //public GameObject cable_2;

    public Color newColor;
    public Color oldColor;

    //Variable públicas que permite continuar cambiando de color el siguiente cable
    //public Hola holaamigo;

    //Variables públicas que permiten identificar la función de botones y/o switches
    public bool primero;
    public bool miswitch;
    public bool goRight;

    public UnityEvent event_cambioDir;

    private void Start()
    {
        Onlight.instance.isOn = true;
        CambioColor(newColor, cable_1);
    }

    void OnMouseDown()
    {
        if (primero)
        {
            if (Input.GetMouseButtonDown(0) && Onlight.instance.isOn == false)
            {
                //event_cambioDir?.Invoke();
                Onlight.instance.isOn = true;
                CambioColor(newColor, cable_1);
                Debug.Log("Click");
                //holaamigo.RecibeMensaje(Onlight.instance.isOn);
            }
        }

        //if (miswitch && Onlight.instance.isOn == true)
        //{
        //    goRight = !goRight;

        //    if (goRight == true)
        //    {
        //        event_cambioDir?.Invoke();
        //        CambioColor(newColor, cable_1);
        //        CambioColor(oldColor, cable_2);
        //    }
        //    else if (goRight == false)
        //    {
        //        event_cambioDir?.Invoke();
        //        CambioColor(newColor, cable_2);
        //        CambioColor(oldColor, cable_1);
        //    }
        //}
    }

    //public void RecibeMensaje(bool value)
    //{
    //    Onlight.instance.isOn = value;

    //    if (goRight == true)
    //    {
    //        Renderer[] renders = cable_1.GetComponentsInChildren<Renderer>();
    //        //Por cada elemento en el array, cambia el color de los cable_1.
    //        foreach (Renderer r in renders)
    //        {
    //            if (Onlight.instance.isOn)
    //            {
    //                r.material.color = newColor;
    //            }
    //            else
    //            {
    //                r.material.color = oldColor;
    //            }
    //        }
    //    }
    //    else if (goRight == false)
    //    {
    //        //Por cada elemento en el array, cambia el color de los cable_2.
    //        Renderer[] renders = cable_2.GetComponentsInChildren<Renderer>();
    //        //Por cada elemento en el array, cambia el color de los cable_1.
    //        foreach (Renderer r in renders)
    //        {
    //            if (Onlight.instance.isOn)
    //            {
    //                r.material.color = newColor;
    //            }
    //            else
    //            {
    //                r.material.color = oldColor;
    //            }
    //        }
    //    }

    //    if (holaamigo != null)
    //    {
    //        holaamigo.RecibeMensaje(Onlight.instance.isOn);
    //    }

    //}

    //Este es el void que permite el cambio de color de los cables en miswitch.
   public void CambioColor(Color color, GameObject cable)
    {
        Renderer[] renders = cable.GetComponentsInChildren<Renderer>();
        foreach (Renderer r in renders)
        {
            r.material.color = color;
        }
    }
}
