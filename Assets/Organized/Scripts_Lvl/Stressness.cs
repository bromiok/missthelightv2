﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stressness : MonoBehaviour
{
    public static Stressness instance;

    public int s_level;

    public void Awake()
    {
        instance = this;
    }

    public void Start()
    {
        s_level = 5;
    }

    private void Update()
    {

        StressLevel();
    }

    void StressLevel()
    {
        if (s_level == 2)
        {
            EnemyController.instance.speed = 4f;
        }
        if (s_level == 1)
        {
            EnemyController.instance.speed = 5f;
        }
        if (s_level == 0)
        {
            GameManager.instance.ReloadScene();
            
        }
    }
}
