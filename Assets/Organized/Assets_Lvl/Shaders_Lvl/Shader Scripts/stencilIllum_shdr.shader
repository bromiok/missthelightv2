Shader "MissTheLight/Stencil Illumination"
{
    Properties
    {
        
		
		[Enum(UnityEngine.Rendering.CompareFunction)] _StencilCompLight("Light Compare", Int) = 3
		_LightID ("Light Pass ID", int) = 1
		_LightText ("Light Texture", 2D) = "white" {}
		
		[Enum(UnityEngine.Rendering.CompareFunction)] _StencilCompDark("Dark Compare", Int) = 3
		_DarkID ("Dark Pass ID", int) = 1
		_DarkText ("Dark Texture", 2D) = "black" {}


		/*[Enum(UnityEngine.Rendering.CompareFunction)] _StencilCompWorn("Worn Compare", Int) = 3
		_WornID ("Worn Pass ID", int) = 2
		_WornText ("Worn Texture", 2D) = "blue" {}*/
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        
        Pass
        {

			Stencil{
			Ref [_LightID]
			Comp [_StencilCompLight]
			}

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
           
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
               
                float4 vertex : SV_POSITION;
            };

            sampler2D _LightText;
            float4 _LightText_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				fixed2 lightUVs = TRANSFORM_TEX(i.uv, _LightText);
				fixed4 finalLight = tex2D(_LightText,lightUVs) +.1;
                return finalLight;
            }
            ENDCG
        }

		  Pass
        {

			Stencil{
			Ref [_DarkID]
			Comp [_StencilCompDark]
			}

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
           
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
               
                float4 vertex : SV_POSITION;
            };

			sampler2D _DarkText;
            float4 _DarkText_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {

				fixed2 darkUVs = TRANSFORM_TEX(i.uv, _DarkText);
				fixed4 finalDark = tex2D(_DarkText,darkUVs) -.15;
                return finalDark;
          
            }
            ENDCG
        }

          /*Pass
        {

			Stencil{
			Ref [_WornID]
			Comp [_StencilCompWorn]
			}

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
           
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
               
                float4 vertex : SV_POSITION;
            };

			sampler2D _WornText;
            float4 _WornText_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {

				fixed2 wornUVs = TRANSFORM_TEX(i.uv, _WornText);
				fixed4 finalWorn = tex2D(_WornText,wornUVs);
                return finalWorn;
          
            }
            ENDCG
        }*/
    }
}
