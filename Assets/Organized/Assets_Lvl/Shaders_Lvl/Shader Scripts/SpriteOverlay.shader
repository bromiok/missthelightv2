﻿Shader "MissTheLight/Sprite Overlay"
{
    Properties
    {
        [PerRendererData] _MainTex ("Texture", 2D) = "white" {}
        _OverlayTex ("Overlay Texture", 2D) = "black" {}
        _SpeedU ("U Speed",int) = 0
        _SpeedV ("V Speed",int) = 0
    }
    SubShader
    {
//Cull Off ZWrite Off ZTest Always
        Blend SrcAlpha OneMinusSrcAlpha
        //Blend Zero One

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };
            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }
            sampler2D _MainTex,_OverlayTex;
            fixed4 _OverlayTex_ST;
            fixed _SpeedU,_SpeedV;
            fixed4 frag (v2f i) : SV_Target
            {
                fixed speedU = _Time.y*_SpeedU/10;
                fixed speedV  = _Time.y*_SpeedV/10;
                fixed4 col = tex2D(_MainTex, i.uv);
                fixed2 overlayUVs = TRANSFORM_TEX(fixed2(i.uv.x +speedU,i.uv.y+speedV),_OverlayTex);
                fixed4 overlayTexture = tex2D(_OverlayTex,overlayUVs);
                fixed4 finalCol = col*overlayTexture;
                clip(finalCol.a - 0.1);
                return finalCol;
            }
            ENDCG
        }
    }
}