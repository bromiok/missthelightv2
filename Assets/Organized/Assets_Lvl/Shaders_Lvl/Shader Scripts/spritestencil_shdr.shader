﻿Shader "MissTheLight/Sprite Illumination"
{
    Properties
    {
        
        _MainTex ("Texture", 2D) = "white" {}
		
		[Enum(UnityEngine.Rendering.CompareFunction)] _StencilCompLight("Light Compare", Int) = 3
		_LightID ("Light Pass ID", int) = 1
		
		[Enum(UnityEngine.Rendering.CompareFunction)] _StencilCompDark("Dark Compare", Int) = 3
		_DarkID ("Dark Pass ID", int) = 1
		_DarkText ("Dark Texture", 2D) = "black" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        
        Pass
        {

			Stencil{
			Ref [_LightID]
			Comp [_StencilCompLight]
			}

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
           
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
               
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
    
         
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				fixed4 col = tex2D(_MainTex, i.uv) +.1;
                return col ;
            }
            ENDCG
        }

		  Pass
        {

			Stencil{
			Ref [_DarkID]
			Comp [_StencilCompDark]
			}

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
           
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
               
                float4 vertex : SV_POSITION;
            };

			sampler2D _MainTex;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {

				fixed4 col = tex2D(_MainTex, i.uv) -.3;
                return col ;
          
            }
            ENDCG
        }
    }
}
