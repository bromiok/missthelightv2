﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotable : MonoBehaviour
{
    public GameObject rotableLight;
    public bool rotable;

    private float rotation;

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "player")
        {
            if (Input.GetKeyDown(KeyCode.E) && rotable == true)
            {
                rotation = 45;
                rotableLight.transform.Rotate(Vector3.up * rotation);
            }
        }
    }
}
